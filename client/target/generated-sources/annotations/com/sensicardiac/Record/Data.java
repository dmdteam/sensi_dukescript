package com.sensicardiac.Record;
import net.java.html.json.*;
public final class Data implements Cloneable {
  private static final Html4JavaType TYPE = new Html4JavaType();
  private final org.netbeans.html.json.spi.Proto proto;
  private final java.util.List<BTdev> prop_BTList;
  public java.util.List<BTdev> getBTList() {
    proto.accessProperty("BTList");
    return prop_BTList;
  }
  private int prop_stet1;
  public int getStet1() {
    proto.accessProperty("stet1");
    return prop_stet1;
  }
  public void setStet1(int v) {
    proto.verifyUnlocked();
    Object o = prop_stet1;
    if (TYPE.isSame(o , v)) return;
    prop_stet1 = v;
    proto.valueHasMutated("stet1", o, v);
  }
  private final java.util.List<java.lang.String> prop_selected;
  public java.util.List<java.lang.String> getSelected() {
    proto.accessProperty("selected");
    return prop_selected;
  }
  private java.lang.String prop_stet_connected_label;
  public java.lang.String getStet_connected_label() {
    proto.accessProperty("stet_connected_label");
    return prop_stet_connected_label;
  }
  public void setStet_connected_label(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_stet_connected_label;
    if (TYPE.isSame(o , v)) return;
    prop_stet_connected_label = v;
    proto.valueHasMutated("stet_connected_label", o, v);
  }
  private java.lang.String prop_data_write_sec;
  public java.lang.String getData_write_sec() {
    proto.accessProperty("data_write_sec");
    return prop_data_write_sec;
  }
  public void setData_write_sec(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_data_write_sec;
    if (TYPE.isSame(o , v)) return;
    prop_data_write_sec = v;
    proto.valueHasMutated("data_write_sec", o, v);
  }
  private java.lang.String prop_graph_data_x;
  public java.lang.String getGraph_data_x() {
    proto.accessProperty("graph_data_x");
    return prop_graph_data_x;
  }
  public void setGraph_data_x(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_graph_data_x;
    if (TYPE.isSame(o , v)) return;
    prop_graph_data_x = v;
    proto.valueHasMutated("graph_data_x", o, v);
  }
  private java.lang.String prop_graph_data_y;
  public java.lang.String getGraph_data_y() {
    proto.accessProperty("graph_data_y");
    return prop_graph_data_y;
  }
  public void setGraph_data_y(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_graph_data_y;
    if (TYPE.isSame(o , v)) return;
    prop_graph_data_y = v;
    proto.valueHasMutated("graph_data_y", o, v);
  }
  private boolean prop_data_get;
  public boolean isData_get() {
    proto.accessProperty("data_get");
    return prop_data_get;
  }
  public void setData_get(boolean v) {
    proto.verifyUnlocked();
    Object o = prop_data_get;
    if (TYPE.isSame(o , v)) return;
    prop_data_get = v;
    proto.valueHasMutated("data_get", o, v);
  }
  private int prop_progress_updates;
  public int getProgress_updates() {
    proto.accessProperty("progress_updates");
    return prop_progress_updates;
  }
  public void setProgress_updates(int v) {
    proto.verifyUnlocked();
    Object o = prop_progress_updates;
    if (TYPE.isSame(o , v)) return;
    prop_progress_updates = v;
    proto.valueHasMutated("progress_updates", o, v);
  }
  private java.lang.String prop_bt_icon;
  public java.lang.String getBt_icon() {
    proto.accessProperty("bt_icon");
    return prop_bt_icon;
  }
  public void setBt_icon(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_bt_icon;
    if (TYPE.isSame(o , v)) return;
    prop_bt_icon = v;
    proto.valueHasMutated("bt_icon", o, v);
  }
  private java.lang.String prop_play_back_method;
  public java.lang.String getPlay_back_method() {
    proto.accessProperty("play_back_method");
    return prop_play_back_method;
  }
  public void setPlay_back_method(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_play_back_method;
    if (TYPE.isSame(o , v)) return;
    prop_play_back_method = v;
    proto.valueHasMutated("play_back_method", o, v);
  }
  private java.lang.String prop_ui_User_name;
  public java.lang.String getUi_User_name() {
    proto.accessProperty("ui_User_name");
    return prop_ui_User_name;
  }
  public void setUi_User_name(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_ui_User_name;
    if (TYPE.isSame(o , v)) return;
    prop_ui_User_name = v;
    proto.valueHasMutated("ui_User_name", o, v);
  }
  private java.lang.String prop_ui_User_Sub;
  public java.lang.String getUi_User_Sub() {
    proto.accessProperty("ui_User_Sub");
    return prop_ui_User_Sub;
  }
  public void setUi_User_Sub(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_ui_User_Sub;
    if (TYPE.isSame(o , v)) return;
    prop_ui_User_Sub = v;
    proto.valueHasMutated("ui_User_Sub", o, v);
  }
  private java.lang.String prop_ui_User_Acc;
  public java.lang.String getUi_User_Acc() {
    proto.accessProperty("ui_User_Acc");
    return prop_ui_User_Acc;
  }
  public void setUi_User_Acc(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_ui_User_Acc;
    if (TYPE.isSame(o , v)) return;
    prop_ui_User_Acc = v;
    proto.valueHasMutated("ui_User_Acc", o, v);
  }
  private java.lang.String prop_ui_User_Proc;
  public java.lang.String getUi_User_Proc() {
    proto.accessProperty("ui_User_Proc");
    return prop_ui_User_Proc;
  }
  public void setUi_User_Proc(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_ui_User_Proc;
    if (TYPE.isSame(o , v)) return;
    prop_ui_User_Proc = v;
    proto.valueHasMutated("ui_User_Proc", o, v);
  }
  private java.lang.String prop_ui_User_Expire;
  public java.lang.String getUi_User_Expire() {
    proto.accessProperty("ui_User_Expire");
    return prop_ui_User_Expire;
  }
  public void setUi_User_Expire(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_ui_User_Expire;
    if (TYPE.isSame(o , v)) return;
    prop_ui_User_Expire = v;
    proto.valueHasMutated("ui_User_Expire", o, v);
  }
  private static Class<DataModel> modelFor() { return null; }
  private Data(net.java.html.BrwsrCtx context) {
    this.proto = TYPE.createProto(this, context);
    this.prop_BTList = proto.createList("BTList", -1);
    this.prop_selected = proto.createList("selected", -1);
  };
  public Data() {
    this(net.java.html.BrwsrCtx.findDefault(Data.class));
  };
  public Data(int aStet1, java.lang.String aStet_connected_label, java.lang.String aData_write_sec, java.lang.String aGraph_data_x, java.lang.String aGraph_data_y, boolean aData_get, int aProgress_updates, java.lang.String aBt_icon, java.lang.String aPlay_back_method, java.lang.String aUi_User_name, java.lang.String aUi_User_Sub, java.lang.String aUi_User_Acc, java.lang.String aUi_User_Proc, java.lang.String aUi_User_Expire, BTdev... aBTList) {
    this(net.java.html.BrwsrCtx.findDefault(Data.class));
    this.prop_stet1 = aStet1;
    this.prop_stet_connected_label = aStet_connected_label;
    this.prop_data_write_sec = aData_write_sec;
    this.prop_graph_data_x = aGraph_data_x;
    this.prop_graph_data_y = aGraph_data_y;
    this.prop_data_get = aData_get;
    this.prop_progress_updates = aProgress_updates;
    this.prop_bt_icon = aBt_icon;
    this.prop_play_back_method = aPlay_back_method;
    this.prop_ui_User_name = aUi_User_name;
    this.prop_ui_User_Sub = aUi_User_Sub;
    this.prop_ui_User_Acc = aUi_User_Acc;
    this.prop_ui_User_Proc = aUi_User_Proc;
    this.prop_ui_User_Expire = aUi_User_Expire;
    proto.initTo(this.prop_BTList, aBTList);
  };
  private static class Html4JavaType extends org.netbeans.html.json.spi.Proto.Type<Data> {
    private Html4JavaType() {
      super(Data.class, DataModel.class, 16, 6);
      registerProperty("BTList", 0, false);
      registerProperty("stet1", 1, false);
      registerProperty("selected", 2, false);
      registerProperty("stet_connected_label", 3, false);
      registerProperty("data_write_sec", 4, false);
      registerProperty("graph_data_x", 5, false);
      registerProperty("graph_data_y", 6, false);
      registerProperty("data_get", 7, false);
      registerProperty("progress_updates", 8, false);
      registerProperty("bt_icon", 9, false);
      registerProperty("play_back_method", 10, false);
      registerProperty("ui_User_name", 11, false);
      registerProperty("ui_User_Sub", 12, false);
      registerProperty("ui_User_Acc", 13, false);
      registerProperty("ui_User_Proc", 14, false);
      registerProperty("ui_User_Expire", 15, false);
      registerFunction("Stet_name_val", 0);
      registerFunction("Record_Function", 1);
      registerFunction("Play_Function", 2);
      registerFunction("Stet_pair", 3);
      registerFunction("User_Info", 4);
      registerFunction("Login_Function", 5);
    }
    @Override public void setValue(Data data, int type, Object value) {
      switch (type) {
        case 0: TYPE.replaceValue(data.getBTList(), BTdev.class, value); return;
        case 1: data.setStet1(TYPE.extractValue(java.lang.Integer.class, value)); return;
        case 2: TYPE.replaceValue(data.getSelected(), java.lang.String.class, value); return;
        case 3: data.setStet_connected_label(TYPE.extractValue(java.lang.String.class, value)); return;
        case 4: data.setData_write_sec(TYPE.extractValue(java.lang.String.class, value)); return;
        case 5: data.setGraph_data_x(TYPE.extractValue(java.lang.String.class, value)); return;
        case 6: data.setGraph_data_y(TYPE.extractValue(java.lang.String.class, value)); return;
        case 7: data.setData_get(TYPE.extractValue(java.lang.Boolean.class, value)); return;
        case 8: data.setProgress_updates(TYPE.extractValue(java.lang.Integer.class, value)); return;
        case 9: data.setBt_icon(TYPE.extractValue(java.lang.String.class, value)); return;
        case 10: data.setPlay_back_method(TYPE.extractValue(java.lang.String.class, value)); return;
        case 11: data.setUi_User_name(TYPE.extractValue(java.lang.String.class, value)); return;
        case 12: data.setUi_User_Sub(TYPE.extractValue(java.lang.String.class, value)); return;
        case 13: data.setUi_User_Acc(TYPE.extractValue(java.lang.String.class, value)); return;
        case 14: data.setUi_User_Proc(TYPE.extractValue(java.lang.String.class, value)); return;
        case 15: data.setUi_User_Expire(TYPE.extractValue(java.lang.String.class, value)); return;
      }
      throw new UnsupportedOperationException();
    }
    @Override public Object getValue(Data data, int type) {
      switch (type) {
        case 0: return data.getBTList();
        case 1: return data.getStet1();
        case 2: return data.getSelected();
        case 3: return data.getStet_connected_label();
        case 4: return data.getData_write_sec();
        case 5: return data.getGraph_data_x();
        case 6: return data.getGraph_data_y();
        case 7: return data.isData_get();
        case 8: return data.getProgress_updates();
        case 9: return data.getBt_icon();
        case 10: return data.getPlay_back_method();
        case 11: return data.getUi_User_name();
        case 12: return data.getUi_User_Sub();
        case 13: return data.getUi_User_Acc();
        case 14: return data.getUi_User_Proc();
        case 15: return data.getUi_User_Expire();
      }
      throw new UnsupportedOperationException();
    }
    @Override public void call(Data model, int type, Object data, Object ev) throws Exception {
      switch (type) {
        case 0:
          com.sensicardiac.Record.DataModel.Stet_name_val();
          return;
        case 1:
          com.sensicardiac.Record.DataModel.Record_Function();
          return;
        case 2:
          com.sensicardiac.Record.DataModel.Play_Function();
          return;
        case 3:
          com.sensicardiac.Record.DataModel.Stet_pair();
          return;
        case 4:
          com.sensicardiac.Record.DataModel.User_Info();
          return;
        case 5:
          com.sensicardiac.Record.DataModel.Login_Function();
          return;
      }
      throw new UnsupportedOperationException();
    }
    @Override public org.netbeans.html.json.spi.Proto protoFor(Object obj) {
      return ((Data)obj).proto;    }
    @Override public void onChange(Data model, int type) {
      switch (type) {
    }
      throw new UnsupportedOperationException();
    }
  @Override public void onMessage(Data model, int index, int type, Object data, Object[] params) {
    switch (index) {
    }
    throw new UnsupportedOperationException("index: " + index + " type: " + type);
  }
    @Override public Data read(net.java.html.BrwsrCtx c, Object json) { return new Data(c, json); }
    @Override public Data cloneTo(Data o, net.java.html.BrwsrCtx c) { return o.clone(c); }
  }
  private Data(net.java.html.BrwsrCtx c, Object json) {
    this(c);
    Object[] ret = new Object[16];
    proto.extract(json, new String[] {
      "BTList",
      "stet1",
      "selected",
      "stet_connected_label",
      "data_write_sec",
      "graph_data_x",
      "graph_data_y",
      "data_get",
      "progress_updates",
      "bt_icon",
      "play_back_method",
      "ui_User_name",
      "ui_User_Sub",
      "ui_User_Acc",
      "ui_User_Proc",
      "ui_User_Expire",
    }, ret);
    for (Object e : useAsArray(ret[0])) {
      this.prop_BTList.add(proto.read(BTdev.class, e));
    }
    this.prop_stet1 = ret[1] == null ? 0 : (TYPE.numberValue(ret[1])).intValue();
    for (Object e : useAsArray(ret[2])) {
        this.prop_selected.add((java.lang.String)e);
    }
    this.prop_stet_connected_label = (java.lang.String)ret[3];
    this.prop_data_write_sec = (java.lang.String)ret[4];
    this.prop_graph_data_x = (java.lang.String)ret[5];
    this.prop_graph_data_y = (java.lang.String)ret[6];
    this.prop_data_get = ret[7] == null ? false : (TYPE.boolValue(ret[7])).booleanValue();
    this.prop_progress_updates = ret[8] == null ? 0 : (TYPE.numberValue(ret[8])).intValue();
    this.prop_bt_icon = (java.lang.String)ret[9];
    this.prop_play_back_method = (java.lang.String)ret[10];
    this.prop_ui_User_name = (java.lang.String)ret[11];
    this.prop_ui_User_Sub = (java.lang.String)ret[12];
    this.prop_ui_User_Acc = (java.lang.String)ret[13];
    this.prop_ui_User_Proc = (java.lang.String)ret[14];
    this.prop_ui_User_Expire = (java.lang.String)ret[15];
  }
  private static Object[] useAsArray(Object o) {
    return o instanceof Object[] ? ((Object[])o) : o == null ? new Object[0] : new Object[] { o };
  }
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append('{');
    sb.append('"').append("BTList").append('"').append(":");
    sb.append(TYPE.toJSON(prop_BTList));
    sb.append(',');
    sb.append('"').append("stet1").append('"').append(":");
    sb.append(TYPE.toJSON(prop_stet1));
    sb.append(',');
    sb.append('"').append("selected").append('"').append(":");
    sb.append(TYPE.toJSON(prop_selected));
    sb.append(',');
    sb.append('"').append("stet_connected_label").append('"').append(":");
    sb.append(TYPE.toJSON(prop_stet_connected_label));
    sb.append(',');
    sb.append('"').append("data_write_sec").append('"').append(":");
    sb.append(TYPE.toJSON(prop_data_write_sec));
    sb.append(',');
    sb.append('"').append("graph_data_x").append('"').append(":");
    sb.append(TYPE.toJSON(prop_graph_data_x));
    sb.append(',');
    sb.append('"').append("graph_data_y").append('"').append(":");
    sb.append(TYPE.toJSON(prop_graph_data_y));
    sb.append(',');
    sb.append('"').append("data_get").append('"').append(":");
    sb.append(TYPE.toJSON(prop_data_get));
    sb.append(',');
    sb.append('"').append("progress_updates").append('"').append(":");
    sb.append(TYPE.toJSON(prop_progress_updates));
    sb.append(',');
    sb.append('"').append("bt_icon").append('"').append(":");
    sb.append(TYPE.toJSON(prop_bt_icon));
    sb.append(',');
    sb.append('"').append("play_back_method").append('"').append(":");
    sb.append(TYPE.toJSON(prop_play_back_method));
    sb.append(',');
    sb.append('"').append("ui_User_name").append('"').append(":");
    sb.append(TYPE.toJSON(prop_ui_User_name));
    sb.append(',');
    sb.append('"').append("ui_User_Sub").append('"').append(":");
    sb.append(TYPE.toJSON(prop_ui_User_Sub));
    sb.append(',');
    sb.append('"').append("ui_User_Acc").append('"').append(":");
    sb.append(TYPE.toJSON(prop_ui_User_Acc));
    sb.append(',');
    sb.append('"').append("ui_User_Proc").append('"').append(":");
    sb.append(TYPE.toJSON(prop_ui_User_Proc));
    sb.append(',');
    sb.append('"').append("ui_User_Expire").append('"').append(":");
    sb.append(TYPE.toJSON(prop_ui_User_Expire));
    sb.append('}');
    return sb.toString();
  }
  public Data clone() {
    return clone(proto.getContext());
  }
  private Data clone(net.java.html.BrwsrCtx ctx) {
    Data ret = new Data(ctx);
    proto.cloneList(ret.getBTList(), ctx, prop_BTList);
    ret.prop_stet1 = getStet1();
    proto.cloneList(ret.getSelected(), ctx, prop_selected);
    ret.prop_stet_connected_label = getStet_connected_label();
    ret.prop_data_write_sec = getData_write_sec();
    ret.prop_graph_data_x = getGraph_data_x();
    ret.prop_graph_data_y = getGraph_data_y();
    ret.prop_data_get = isData_get();
    ret.prop_progress_updates = getProgress_updates();
    ret.prop_bt_icon = getBt_icon();
    ret.prop_play_back_method = getPlay_back_method();
    ret.prop_ui_User_name = getUi_User_name();
    ret.prop_ui_User_Sub = getUi_User_Sub();
    ret.prop_ui_User_Acc = getUi_User_Acc();
    ret.prop_ui_User_Proc = getUi_User_Proc();
    ret.prop_ui_User_Expire = getUi_User_Expire();
    return ret;
  }
  /** Activates this model instance in the current {@link 
net.java.html.json.Models#bind(java.lang.Object, net.java.html.BrwsrCtx) browser context}. 
In case of using Knockout technology, this means to 
bind JSON like data in this model instance with Knockout tags in 
the surrounding HTML page.
This method binds to element '""' on the page
@return <code>this</code> object
*/
  public Data applyBindings() {
    proto.applyBindings();
    return this;
  }
  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof Data)) return false;
    Data p = (Data)o;
    if (!TYPE.isSame(prop_BTList, p.prop_BTList)) return false;
    if (!TYPE.isSame(prop_stet1, p.prop_stet1)) return false;
    if (!TYPE.isSame(prop_selected, p.prop_selected)) return false;
    if (!TYPE.isSame(prop_stet_connected_label, p.prop_stet_connected_label)) return false;
    if (!TYPE.isSame(prop_data_write_sec, p.prop_data_write_sec)) return false;
    if (!TYPE.isSame(prop_graph_data_x, p.prop_graph_data_x)) return false;
    if (!TYPE.isSame(prop_graph_data_y, p.prop_graph_data_y)) return false;
    if (!TYPE.isSame(prop_data_get, p.prop_data_get)) return false;
    if (!TYPE.isSame(prop_progress_updates, p.prop_progress_updates)) return false;
    if (!TYPE.isSame(prop_bt_icon, p.prop_bt_icon)) return false;
    if (!TYPE.isSame(prop_play_back_method, p.prop_play_back_method)) return false;
    if (!TYPE.isSame(prop_ui_User_name, p.prop_ui_User_name)) return false;
    if (!TYPE.isSame(prop_ui_User_Sub, p.prop_ui_User_Sub)) return false;
    if (!TYPE.isSame(prop_ui_User_Acc, p.prop_ui_User_Acc)) return false;
    if (!TYPE.isSame(prop_ui_User_Proc, p.prop_ui_User_Proc)) return false;
    if (!TYPE.isSame(prop_ui_User_Expire, p.prop_ui_User_Expire)) return false;
    return true;
  }
  public int hashCode() {
    int h = Data.class.getName().hashCode();
    h = TYPE.hashPlus(prop_BTList, h);
    h = TYPE.hashPlus(prop_stet1, h);
    h = TYPE.hashPlus(prop_selected, h);
    h = TYPE.hashPlus(prop_stet_connected_label, h);
    h = TYPE.hashPlus(prop_data_write_sec, h);
    h = TYPE.hashPlus(prop_graph_data_x, h);
    h = TYPE.hashPlus(prop_graph_data_y, h);
    h = TYPE.hashPlus(prop_data_get, h);
    h = TYPE.hashPlus(prop_progress_updates, h);
    h = TYPE.hashPlus(prop_bt_icon, h);
    h = TYPE.hashPlus(prop_play_back_method, h);
    h = TYPE.hashPlus(prop_ui_User_name, h);
    h = TYPE.hashPlus(prop_ui_User_Sub, h);
    h = TYPE.hashPlus(prop_ui_User_Acc, h);
    h = TYPE.hashPlus(prop_ui_User_Proc, h);
    h = TYPE.hashPlus(prop_ui_User_Expire, h);
    return h;
  }
}
