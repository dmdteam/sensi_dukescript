package com.sensicardiac.Record;
import net.java.html.json.*;
public final class BTdev implements Cloneable {
  private static final Html4JavaType TYPE = new Html4JavaType();
  private final org.netbeans.html.json.spi.Proto proto;
  private java.lang.String prop_serial;
  public java.lang.String getSerial() {
    proto.accessProperty("serial");
    return prop_serial;
  }
  public void setSerial(java.lang.String v) {
    proto.verifyUnlocked();
    Object o = prop_serial;
    if (TYPE.isSame(o , v)) return;
    prop_serial = v;
    proto.valueHasMutated("serial", o, v);
  }
  private static Class<DataModel.BTModel> modelFor() { return null; }
  private BTdev(net.java.html.BrwsrCtx context) {
    this.proto = TYPE.createProto(this, context);
  };
  public BTdev() {
    this(net.java.html.BrwsrCtx.findDefault(BTdev.class));
  };
  public BTdev(java.lang.String aSerial) {
    this(net.java.html.BrwsrCtx.findDefault(BTdev.class));
    this.prop_serial = aSerial;
  };
  private static class Html4JavaType extends org.netbeans.html.json.spi.Proto.Type<BTdev> {
    private Html4JavaType() {
      super(BTdev.class, DataModel.BTModel.class, 1, 0);
      registerProperty("serial", 0, false);
    }
    @Override public void setValue(BTdev data, int type, Object value) {
      switch (type) {
        case 0: data.setSerial(TYPE.extractValue(java.lang.String.class, value)); return;
      }
      throw new UnsupportedOperationException();
    }
    @Override public Object getValue(BTdev data, int type) {
      switch (type) {
        case 0: return data.getSerial();
      }
      throw new UnsupportedOperationException();
    }
    @Override public void call(BTdev model, int type, Object data, Object ev) throws Exception {
      switch (type) {
      }
      throw new UnsupportedOperationException();
    }
    @Override public org.netbeans.html.json.spi.Proto protoFor(Object obj) {
      return ((BTdev)obj).proto;    }
    @Override public void onChange(BTdev model, int type) {
      switch (type) {
    }
      throw new UnsupportedOperationException();
    }
  @Override public void onMessage(BTdev model, int index, int type, Object data, Object[] params) {
    switch (index) {
    }
    throw new UnsupportedOperationException("index: " + index + " type: " + type);
  }
    @Override public BTdev read(net.java.html.BrwsrCtx c, Object json) { return new BTdev(c, json); }
    @Override public BTdev cloneTo(BTdev o, net.java.html.BrwsrCtx c) { return o.clone(c); }
  }
  private BTdev(net.java.html.BrwsrCtx c, Object json) {
    this(c);
    Object[] ret = new Object[1];
    proto.extract(json, new String[] {
      "serial",
    }, ret);
    this.prop_serial = (java.lang.String)ret[0];
  }
  private static Object[] useAsArray(Object o) {
    return o instanceof Object[] ? ((Object[])o) : o == null ? new Object[0] : new Object[] { o };
  }
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append('{');
    sb.append('"').append("serial").append('"').append(":");
    sb.append(TYPE.toJSON(prop_serial));
    sb.append('}');
    return sb.toString();
  }
  public BTdev clone() {
    return clone(proto.getContext());
  }
  private BTdev clone(net.java.html.BrwsrCtx ctx) {
    BTdev ret = new BTdev(ctx);
    ret.prop_serial = getSerial();
    return ret;
  }
  private BTdev applyBindings() {
    throw new IllegalStateException("Please specify targetId=\"\" in your @Model annotation");
  }
  public boolean equals(Object o) {
    if (o == this) return true;
    if (!(o instanceof BTdev)) return false;
    BTdev p = (BTdev)o;
    if (!TYPE.isSame(prop_serial, p.prop_serial)) return false;
    return true;
  }
  public int hashCode() {
    int h = BTdev.class.getName().hashCode();
    h = TYPE.hashPlus(prop_serial, h);
    return h;
  }
}
