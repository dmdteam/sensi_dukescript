package sensi_rest_api.entities;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Edward on 15/06/10.
 */
public class Patient {
    @SerializedName("country")
    public String country;

    @SerializedName("addressLine1")
    public String addressLine1;

    @SerializedName("addressLine2")
    public String addressLine2;

    @SerializedName("city")
    public String city;

    @SerializedName("email")
    public String email;

    @SerializedName("firstName")
    public String firstName;

    @SerializedName("lastName")
    public String lastName;


    public String getNameStr() {
        return lastName + ", " + firstName;
    }


    @SerializedName("mobileNumber")
    public String mobileNumber;

    @SerializedName("postalCode")
    public String postalCode;

    @SerializedName("suburb")
    public String suburb;

    @SerializedName("telephoneNumber")
    public String telephoneNumber;

    @SerializedName("dateOfBirth")
    public long dateOfBirth;

    public Date getDateofBirth()
    {
        return new Date(dateOfBirth);
    }

    public void setDateofBirth(Date dob)
    {
        dateOfBirth = dob.getTime();
    }

    public String getDateofBirthStr()
    {
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        return format.format(new Date(dateOfBirth));
    }

//    public String gender_type;

    @SerializedName("gender")
    public String gender;

    @SerializedName("weight")
    public Double weight;

    @SerializedName("fileId")
    public String fileId;

    @SerializedName("patientType")
    public String patientType;

    @SerializedName("uuid")
    public String uuid;


    @SerializedName("lastSession")
    public long lastSession;

    public Date getLastSessionDate()
    {
        return new Date(lastSession);
    }

    public String getLastSessionDateStr()
    {
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm");
        return format.format(new Date(lastSession));
    }


    @SerializedName("lastSessionUUID")
    public String lastSessionUUID;
//    public enum GenderType {Male, Female, Unknown}
}
