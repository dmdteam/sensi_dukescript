package sensi_rest_api.entities;

/**
 * Created by eugenepretorius on 15/06/18.
 */
public enum DiagnosisType {
    PATHOLOGICAL,
    NORMAL,
    ERROR,
    ABNORMAL,

    PENDING,
    FILE_LOADED,
    PACKAGED,
    SESSION_CREATING,
    SESSION_CREATED,
    SERVER_CALLED,
    NO_PATIENT,

}
