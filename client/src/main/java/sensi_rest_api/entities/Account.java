package sensi_rest_api.entities;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Edward on 15/06/10.
 */
public class Account {

    @SerializedName("username")
    public String username;

    @SerializedName("tokens")
    public int tokens;

    @SerializedName("dueDate")
    public long dueDate;

    @SerializedName("sensiPackage")
    public String sensiPackage;

    @SerializedName("unlimited")
    public Boolean unlimited;

}
