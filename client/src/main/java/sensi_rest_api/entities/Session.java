package sensi_rest_api.entities;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Edward on 15/06/10.
 */
public class Session {
    @SerializedName("patientid")
    public String patientid;

    @SerializedName("recordingDate")
    public long recordingDate;

    @SerializedName("uuid")
    public String uuid;


    @SerializedName("weight")
    public int weight;

    @SerializedName("deviceId")
    public String deviceId;


////    @SerializedName("result")
//    public DiagnosisType result;

    @SerializedName("sessionType")
    public String sessionType;

//    @SerializedName("diagnosisList")
//    public Diagnoses diagnosisList;

    @SerializedName("diagnosisList")
    public DiagnosisList diagnosisList;





    public Date getDateofRecording() {
        return new Date(recordingDate);
    }

    public String getDateofRecordingStr()
    {
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        return format.format(new Date(recordingDate));
    }
}
