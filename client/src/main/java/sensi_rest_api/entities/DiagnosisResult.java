package sensi_rest_api.entities;

/**
 * Created by eugenepretorius on 15/06/18.
 */
public class DiagnosisResult {
    public String posture;
    public Diagnosis diagnosis;

}
