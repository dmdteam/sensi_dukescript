package sensi_rest_api.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Edward on 15/06/10.
 */
public class SessionsList {

    public List<Session> sessions;
    public String patientid;

    public int Count()
    {
        return sessions.size();
    }
}


