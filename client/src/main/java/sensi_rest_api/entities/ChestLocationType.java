package sensi_rest_api.entities;

/**
 * Created by eugenepretorius on 15/06/18.
 */
public enum ChestLocationType {
    AORTIC,
    PULMONARY,
    TRICUSPID,
    MITRAL,
    UNKNWON,
}
