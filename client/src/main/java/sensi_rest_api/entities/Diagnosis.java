package sensi_rest_api.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Edward on 15/06/10.
 */
public class Diagnosis {

    @SerializedName("recordingUUID")
    public String recordingUUID;

    @SerializedName("uuid")
    public String uuid;

    @SerializedName("location")
    public String location;

    @SerializedName("diagnosis")
    private String diagnosis;

    @SerializedName("heartRate")
    public int heartRate;

    @SerializedName("avgR_Rinterval")
    public double avgR_Rinterval;

    @SerializedName("stddR_Rinterval")
    public double stddR_Rinterval;

    @SerializedName("systoleLength")
    public int systoleLength;

    @SerializedName("diastoleLength")
    public int diastoleLength;

    @SerializedName("duration")
    public Object duration;

    @SerializedName("numBeats")
    public int numBeats;

    @SerializedName("confidence")
    public int confidence;

    @SerializedName("systoleStdLength")
    public double systoleStdLength;

    @SerializedName("diastoleStdLength")
    public double diastoleStdLength;
//
//    @SerializedName("s1s2HSignalPeakPos")
//    public int[] s1s2HSignalPeakPos;
//
//    @SerializedName("s1s2s1HSignalPeakPos")
//    public int[] s1s2s1HSignalPeakPos;
//
//    @SerializedName("s1s2HSignalDiagnosis")
//    public int[] s1s2HSignalDiagnosis;
//
////    @SerializedName("version")
////    public String version;
//
////    @SerializedName("formattedRr")
////    public String formattedRr;
//
////    @SerializedName("diagnosisResult")
////    public String diagnosisResult;
//
//
////    @SerializedName("energy")
////    public diagnosis_energy energy;
//
///*    public RecordLocation recordLocation {
//        get { return location == null ? RecordLocation.None : (RecordLocation)Enum.Parse(typeof(RecordLocation), location, true); }
//        set { location = value.ToString(); }
//    }
//*/
//
    public DiagnosisType getDiagnosisType()
    {
        DiagnosisType d = DiagnosisType.ERROR;
        if (diagnosis == null)
            d = DiagnosisType.ERROR;
        else {
            if (diagnosis.equals("PATHOLOGICAL"))   d = DiagnosisType.PATHOLOGICAL;
            if (diagnosis.equals("NORMAL"))         d = DiagnosisType.NORMAL;
            if (diagnosis.equals("ERROR"))          d = DiagnosisType.ERROR;
            if (diagnosis.equals("ABNORMAL"))       d = DiagnosisType.ABNORMAL;
        }

        return d;
    }


    public ChestLocationType getLocationType()
    {
        ChestLocationType d = ChestLocationType.UNKNWON;
        if (location == null)
            d = ChestLocationType.UNKNWON;
        else {
            if (location.equals("AORTIC"))      d = ChestLocationType.AORTIC;
            if (location.equals("PULMONARY"))   d = ChestLocationType.PULMONARY;
            if (location.equals("TRICUSPID"))   d = ChestLocationType.TRICUSPID;
            if (location.equals("MITRAL"))      d = ChestLocationType.MITRAL;
        }

        return d;
    }
}
