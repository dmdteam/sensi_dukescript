package sensi_rest_api.entities;

import android.accounts.Account;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eugenepretorius on 15/06/04.
 */
 public class User
    {
        @SerializedName("country")
        public String country;

        @SerializedName("password")
        public String password;

        @SerializedName("userName")
        public String userName;

        @SerializedName("addressLine1")
        public String addressLine1;

        @SerializedName("addressLine2")
        public String addressLine2;

        @SerializedName("city")
        public String city;

        @SerializedName("email")
        public String email;

        @SerializedName("firstName")
        public String firstName;

        @SerializedName("lastName")
        public String lastName;

        @SerializedName("mobileNumber")
        public String mobileNumber;

        @SerializedName("postalCode")
        public String postalCode;

        @SerializedName("suburb")
        public String suburb;

        @SerializedName("telephoneNumber")
        public String telephoneNumber;

        @SerializedName("salutation")
        public String salutation;

        @SerializedName("registrationDate")
        public long registrationDate;

        @SerializedName("account")
        public Account account;

        @SerializedName("accountLocked")
        public boolean accountLocked;

    }

