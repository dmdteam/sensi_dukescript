package sensi_rest_api.entities;

/**
 * Created by eugenepretorius on 15/06/18.
 */
public class diagnosis_energy {
    public float s1;
    public float s2;

    public float es;
    public float ms;
    public float ls;

    public float ed;
    public float md;
    public float ld;
}
