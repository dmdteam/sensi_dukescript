package sensi_rest_api.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Edward on 15/06/10.
 */
public class Recording {
    @SerializedName("duration")
    public double duration;

    @SerializedName("auscultationLocation")
    public String auscultationLocation;

    @SerializedName("diagnosis")
    public Diagnosis diagnosis;

    @SerializedName("uuid")
    public String uuid;

    @SerializedName("posture")
    public String posture;

    @SerializedName("patientid")
    public String patientid;

    @SerializedName("sound")
    public String sound;

    @SerializedName("lengthSound")
    public int lengthSound;

    @SerializedName("minHR")
    public int minHR;

    @SerializedName("maxHR")
    public int maxHR;

    @SerializedName("sessionid")
    public String sessionid;

    @SerializedName("notes")
    public String notes;

    //public string hsFile;
    //public object ecgFile;
    //public object ecgLocation;
    //public EcgDiagnosis ecgDiagnosis;
    //public object ecg;
    //public object lengthEcg;


    //public RecordLocation auscultationRecordLocation
    //{
    //    get { return auscultationLocation == null ? RecordLocation.None : (RecordLocation)Enum.Parse(typeof(RecordLocation), auscultationLocation, true); }
    //    set { auscultationLocation = value.ToString().ToUpper(); }
    //}
//    public short[] sound_array;
}
