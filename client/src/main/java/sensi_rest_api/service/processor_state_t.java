package sensi_rest_api.service;

/**
 * Created by eugenepretorius on 15/06/25.
 */
public enum processor_state_t {
    IDLE,
    GETTING,
    POSTING,
    DELETING,
    UPDATING,
    TRANSACTION_BUSY,
}