package sensi_rest_api.service;

/**
 * Created by eugenepretorius on 15/06/25.
 */
public class Processor {
    ///The processor sits between the rest-client and service to handle state change information
    processor_state_t state;    /// for each api call update the state
    Object result;              /// store the result on complete and use content to persist
}
