package sensi_rest_api;

/**
 * Created by eugenepretorius on 15/06/04.
 */
public class server_properties {

    public static boolean USE_SSL = true;
    private static String SERVER_PORT = "80";   //SSL PORT = 443
    private static String SERVER_PROTOCOL = "http";  //SSL PROTOCOL = https

    public static String SERVER_IP_ADDRESS = "diacoustic.co.za";

    public static String BASE_URL()
    {
        if (USE_SSL) {
            SERVER_PROTOCOL = "https";
            SERVER_PORT = "443";
        }

        //String test = SERVER_PROTOCOL + "://" + SERVER_IP_ADDRESS+":"+SERVER_PORT + "/sensimob-web/api";
        return SERVER_PROTOCOL + "://" + SERVER_IP_ADDRESS+":"+SERVER_PORT + "/sensimob-web/api";

        //return "http://10.0.0.12/sensimob-web/api/";
//        return "http://176.58.108.166/sensimob-web/api";
    }
}



//    private OkHttpClient configureClient()
//    {
////        http://stackoverflow.com/questions/22957477/sslhandshakeexception-certpathvalidatorexception-on-android-2-3-but-not-on-andro
//        // Load CAs from an InputStream
//// (could be from a resource or ByteArrayInputStream or ...)
////        CertificateFactory cf = CertificateFactory.getInstance("X.509");
////// From https://www.washington.edu/itconnect/security/ca/load-der.crt
////        InputStream caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
////        Certificate ca;
////        try {
////            ca = cf.generateCertificate(caInput);
////            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
////        } finally {
////            caInput.close();
////        }
////
////// Create a KeyStore containing our trusted CAs
////        String keyStoreType = KeyStore.getDefaultType();
////        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
////        keyStore.load(null, null);
////        keyStore.setCertificateEntry("ca", ca);
////
////// Create a TrustManager that trusts the CAs in our KeyStore
////        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
////        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
////        tmf.init(keyStore);
////
////// Create an SSLContext that uses our TrustManager
////        SSLContext context = SSLContext.getInstance("TLS");
////        context.init(null, tmf.getTrustManagers(), null);
////
////// Tell the URLConnection to use a SocketFactory from our SSLContext
////        URL url = new URL("https://certs.cac.washington.edu/CAtest/");
////        HttpsURLConnection urlConnection =
////                (HttpsURLConnection)url.openConnection();
////        urlConnection.setSSLSocketFactory(context.getSocketFactory());
////        InputStream in = urlConnection.getInputStream();
////        copyInputStreamToOutputStream(in, System.out);
//
//
//
//        OkHttpClient client = new OkHttpClient();
//        SSLSocketFactory soc = new SSLSocketFactory() {
//            @Override
//            public String[] getDefaultCipherSuites() {
//                return new String[0];
//            }
//
//            @Override
//            public String[] getSupportedCipherSuites() {
//                return new String[0];
//            }
//
//            @Override
//            public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
//                return null;
//            }
//
//            @Override
//            public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
//                return null;
//            }
//
//            @Override
//            public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
//                return null;
//            }
//
//            @Override
//            public Socket createSocket(InetAddress host, int port) throws IOException {
//                return null;
//            }
//
//            @Override
//            public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
//                return null;
//            }
//        };
//        client.setSslSocketFactory(soc);
//        return client;
//    }