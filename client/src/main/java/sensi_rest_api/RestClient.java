package sensi_rest_api;



import com.sensicardiac.Record.Settings_States;
import com.squareup.okhttp.ConnectionSpec;
import sensi_rest_api.entities.Diagnosis;
import sensi_rest_api.entities.Patient;
import sensi_rest_api.entities.PatientList;
//import sensi_rest_api.entities.Recording;
import sensi_rest_api.entities.Session;
import sensi_rest_api.entities.SessionsList;
import sensi_rest_api.entities.User;
//import sensi_rest_api.util.encoder;
import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.util.Base64;
import java.util.Collections;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.client.Response;
import sensi_rest_api.util.Encode_Android_Base64;
import sensi_rest_api.util.Encode_Java_base64;
//import rx.Observable;
//import rx.Subscription;
//import rx.functions.Action1;
//import rx.functions.Func1;


//import static sensi_rest_api.util.encoder.int_arr_to_string;

/**
 * Created by eugenepretorius on 15/06/10.
 */
public class RestClient {

    private final ISensiAPI mSensiWebService;
    public final User user_active;
    public Patient patient_active;
    public Session session_active;
//    public Recording recording_active;
    public Diagnosis diagnosis_active;




    public RestClient(String URL, String username, String password) {
//        https://github.com/square/retrofit/issues/265
//        https://futurestud.io/blog/android-basic-authentication-with-retrofit/
        //http://blog.robinchutaux.com/blog/a-smart-way-to-use-retrofit/

        final String credentials = username + ":" + password;
        user_active = new User();
        user_active.userName = username;

        OkHttpClient client = getUnsafeOkHttpClient();  //configureClient();

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                //String string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                String string = "";
                if (Settings_States.device == 1)
                {
                    string = Encode_Java_base64.Java_base_enc(credentials);
                }
                else if (Settings_States.device == 2)
                {
                    string = Encode_Android_Base64.Android_base_enc(credentials);
                }
                request.addHeader("Accept", "application/json");
                request.addHeader("Authorization", string);
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(server_properties.BASE_URL())
                .setClient(new OkClient(client))
                .setRequestInterceptor(requestInterceptor)
                .build();

        mSensiWebService = restAdapter.create(ISensiAPI.class);
    }


    //TODO: Fix ssl cert authorization
    //  SKIPS ALL THE SSL CERT ERRORS
    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                    }
            };



            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            
            
            if (server_properties.USE_SSL == true) {
                okHttpClient.setSslSocketFactory(sslSocketFactory);
                okHttpClient.setConnectionSpecs(Collections.singletonList(ConnectionSpec.COMPATIBLE_TLS));
                 System.setProperty("jsse.enableSNIExtension", "false");
                okHttpClient.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });
            }
          

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


//    public void onNext(Action1<User> res) {
//
//    }
//    public void onComplete() {
//
//    }
//
//    public void onError(Action1<Throwable> err) {
//
//    }

//    final Action1<? super T> onNext, final Action1<Throwable> onError
//    public Subscription<User> fetchUserRx(final String uname) {
//        return mSensiWebService.fetchUserRx(uname)
//                .subscribe(new Action1<User>() {
//                    @Override
//                    public void call(User user) {
//                        User test = user;
//                    }
//                });
//
////            .subscribe(onComplete, onError);
//
//    }

//    public void fetchUserRx2(final String uname)
//    {
//        Object res = mSensiWebService.fetchUserRx(uname)
//                .subscribe(new Action1<User>() {
//                    @Override
//                    public void call(User user) {
//                        User test = user;
//                    }
//                });
//    }



    public User fetchUser() {
        return mSensiWebService.fetchUser(user_active.userName);
    }


    public PatientList fetchPatients() {
        return mSensiWebService.fetchPatients(user_active.userName);
    }

    public Patient addPatient(final Patient p) {
        return mSensiWebService.addPatient(p);
    }


    public SessionsList fetchSessions(final Patient p) {
        return mSensiWebService.fetchSessionsForPatient(p.uuid);
    }

    public Response fetchSessionsRaw(final Patient p) {
        return mSensiWebService.fetchSessionsForPatientRaw(p.uuid);
    }


    public Session fetchSession(final Session s) {
        return mSensiWebService.fetchSession(s.uuid);
    }

    public Session addSession(final Session s) {
        return mSensiWebService.addSession(s);
    }

    public Session endSession(final Session s) {
        return mSensiWebService.endSession(s.uuid);
    }

//    public Diagnosis addRecording(final Recording r) {
//        return mSensiWebService.addRecording(r);
//    }

//    public Diagnosis addRecordingB64(final Recording r, short[] sound_array) {
//
//        try {
//            String data =  encoder.CompressBZ2andEncodeB64(sound_array);
//            r.sound = data;
//        }
//        catch (Exception e){
//            String msg = e.getMessage();
//            String tmp = msg;
//        }
//
//        return mSensiWebService.addRecordingB64(r);
//    }

//    public Diagnosis addRecordingB64_bypass(final Recording r, byte[] bzip2downloaded) {
//        try {
//            String data =  Base64.encodeToString(bzip2downloaded, 0, bzip2downloaded.length, Base64.DEFAULT);
//            r.sound = data;
//        }
//        catch (Exception e){
//            String msg = e.getMessage();
//            String tmp = msg;
//        }
//
//        return mSensiWebService.addRecordingB64(r);
//    }
//
//    public Recording fetchRecording(final String guid) { return mSensiWebService.fetchRecording(guid); }
//
//    public Recording fetchRecordingB64(final String guid)
//    {
//        Recording rec =  mSensiWebService.fetchRecordingB64(guid);
//
//        try {
//            short[] data =  decoder.readBase64AndDecompressBZ2(rec.sound);
//            rec.sound = int_arr_to_string(data);
//        }
//        catch (Exception e){
//            String msg = e.getMessage();
//            String tmp = msg;
//        }
//
//        return rec;
//    }
//
//    public byte[] fetchRecordingB64_bypass(final String guid)
//    {
//        Recording rec = mSensiWebService.fetchRecordingB64(guid);
//
//        try {
//            return Base64.decode(rec.sound, Base64.DEFAULT);
//        }
//        catch (Exception e){
//            String msg = e.getMessage();
//            String tmp = msg;
//        }
//
//        return new byte[0];
//    }

    // Sending side
//    byte[] data = text.getBytes("UTF-8");
//    String base64 = Base64.encodeToString(data, Base64.DEFAULT);
//




//    private class WeatherDataEnvelope {
//        @SerializedName("cod")
//        private int httpCode;
//
//        class Weather {
//            public String description;
//        }
//
//        /**
//         * The web service always returns a HTTP header code of 200 and communicates errors
//         * through a 'cod' field in the JSON payload of the response body.
//         */
//        public Observable filterWebServiceErrors() {
//            if (httpCode == 200) {
//                return Observable.just(this);
//            } else {
//                return Observable.error(
//                        new HttpException("There was a problem fetching the weather data."));
//            }
//        }
//    }





}
