package sensi_rest_api;

import sensi_rest_api.entities.Patient;
import sensi_rest_api.entities.Session;
import sensi_rest_api.entities.User;
import sensi_rest_api.entities.PatientList;
import sensi_rest_api.entities.SessionsList;
//import com.sensicardiac.sensi_rest_api.entities.Recording;
import sensi_rest_api.entities.Diagnosis;


import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;


/**
 * Created by eugenepretorius on 15/06/04.
 */
public interface ISensiAPI {

    //You can use rx.java for sophisticated composition of requests


    @GET("/version")
    public void fetchVersion(Callback<String> callback);

    @GET("/version")
    public String fetchVersion();



    @GET("/user/{usr}")
    public void fetchUser(@Path("usr") String user, Callback<User> callback); //non blocking call

  //  @GET("/user/{usr}")
  //  public Observable<User> fetchUserRx(@Path("usr") String user); //havent figure it out yet

    @GET("/user/{usr}")
    public User fetchUser(@Path("usr") String user);

    @POST("/user")
    public User addUser(@Body String user);

    @PUT("/user")
    public User updateUser(@Body String user);



    @GET("/patient/list/{user}")
    public PatientList fetchPatients(@Path("user") String user);

    @GET("/patient/{guid}")
    public Patient fetchPatient(@Path("guid") String guid);

    @POST("/patient")
    public Patient addPatient(@Body Patient p);

    @PUT("/patient/{uuid}")
    public Patient updatePatient(@Path("pat") String uuid, @Body Patient p);

    @DELETE("/patient/delete/{guid}")
    public Patient deletePatient(@Path("guid") String guid);




    @GET("/session/list/{patient_guid}")
    public SessionsList fetchSessionsForPatient(@Path("patient_guid") String patient_guid);

    @GET("/session/list/{patient_guid}")
    public Response fetchSessionsForPatientRaw(@Path("patient_guid") String patient_guid);


    @GET("/session/{guid}")
    public Session fetchSession(@Path("guid") String guid);

    @POST("/session")
    public Session addSession(@Body Session s);

    @DELETE("/session/{guid}")
    public Patient deleteSession(@Path("guid") String guid);


    @GET("/session/report?uuid={guid}&offset={off}")
    public Session fetchSessionReport(@Path("guid") String guid, @Path("off") int offset);


    @GET("/session/finalize/{guid}")
    public Session endSession(@Path("guid") String guid);


//
//
//    @GET("/recording/{guid}")
//    //    returns the wav file data in recording.sound
//    public Recording fetchRecording(@Path("guid") String guid);
//
//    @GET("/recording_b64/{guid}")
//    //    returns the wav file data in recording.sound
//    public Recording fetchRecordingB64(@Path("guid") String guid);
//
//
//    @POST("/recording")
//    public Diagnosis addRecording(@Body Recording r);
//
//    @POST("/recording_b64")
//    public Diagnosis addRecordingB64(@Body Recording r);
//
//    @PUT("/recording")
//    public Diagnosis updateRecording(@Body Recording r);
//
//    @PUT("/recording/resetHR")
//    public Diagnosis updateRecordingWithNewHR(@Body Recording r);

    @DELETE("/recording/{uuid}")
    public Diagnosis updateRecordingHR(@Path("guid") String guid);



    @PUT("/diagnosis")
    public Diagnosis updateDiagnosis(@Body Diagnosis d);
}
