/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sensicardiac.Record;

import com.mmm.healthcare.scope.ConfigurationFactory;
import com.mmm.healthcare.scope.IBluetoothManager;
import com.mmm.healthcare.scope.*;
import java.io.File;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Vector;
import sensi_rest_api.entities.PatientList;

/**
 *
 * @author Edward
 */
public class Settings_States {
    public static boolean stet_connect_state = false;
    public static boolean is_Connected = false;
public static String license_string ="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                                 "<License>\n" +
                                 "  <CompanyName>3M Company</CompanyName>\n" +
                                 "  <Hash>DZ08GNbneTu777pfzSukOjBlp3A1brJdRvPgxH9+WXQ=</Hash>\n" +
                                 "</License>";
public static InputStream License_IS = null;
public static IBluetoothManager manager = null;
public static Vector<com.mmm.healthcare.scope.Stethoscope> pairedStethoscopes = null;
public static Iterator itr = null;
public static ConfigurationFactory conf = null;
public static Stethoscope Sender_Stethoscope = null;
public static IStethoscopeListener Stethoscope_Listener;
public static boolean isRecording = false;
    public static int offSet = 0;
    public static byte[] stethoscopeAudioData;
    public static int device = 0;
    public static File wavfile = null;
    //_User_//
    public static String ui_User_Name = "USER NAME";
    public static String ui_User_sub = "SUB";
    public static String ui_User_acc = "Acc";
    public static String ui_User_proc = "Proc";
    public static String ui_User_expire = "Expire";
    public static PatientList pats = null;
}
