package com.sensicardiac.Record;


import static com.sensicardiac.Record.Settings_States.device;
import static com.sensicardiac.Record.Settings_States.wavfile;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Edward
 */
public class Play_Spk {
   public static void Playback_spk(){
      
        if (device == 1){
       AudioInputStream audioStream = null;
        try {
            File audioFile = wavfile;
            audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
            SourceDataLine audioLine = (SourceDataLine) AudioSystem.getLine(info);
            audioLine.open(format);
 
            audioLine.start();
            int BUFFER_SIZE = 4096;
 
            byte[] bytesBuffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
 
            while ((bytesRead = audioStream.read(bytesBuffer)) != -1) {
                audioLine.write(bytesBuffer, 0, bytesRead);
            }
            audioLine.drain();
            audioLine.close();
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(Stethoscope_Commands.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Stethoscope_Commands.class.getName()).log(Level.SEVERE, null, ex);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Stethoscope_Commands.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                
                audioStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Stethoscope_Commands.class.getName()).log(Level.SEVERE, null, ex);
            }
        }System.out.println("Done PLay");
        }
        else if (device == 2){
//            
//         MediaPlayer mp = new MediaPlayer();
//        mp.setVolume(1.0f, 1.0f);
//
//        //throws IO exception
//        try {
//            
//            
//            mp.setDataSource(wavfile.toString());
//            mp.prepare();
//            mp.setLooping(false);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        mp.start();
//
//
//        //----------
//
//        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                //button_voice_record.setVisibility(View.VISIBLE);
//                mp.stop();
//                mp.reset();
//                mp.release();
//               // Playing = false;
//               // button_voice_play.setText("Play");
//
//            }
//        });
            System.out.println("not Yet");
        }
    }
   
  
}
