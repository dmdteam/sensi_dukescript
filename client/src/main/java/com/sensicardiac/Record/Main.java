package com.sensicardiac.Record;

import net.java.html.boot.BrowserBuilder;
import com.sensicardiac.Record.Settings_States;

public final class Main {
    private Main() {
    }

    public static void main(String... args) throws Exception {
        BrowserBuilder.newBrowser().
            loadPage("pages/index.html").
            loadClass(Main.class).
            invoke("onPageLoad", args).
            showAndWait();
        System.exit(0);
    }

    /**
     * Called when the page is ready.
     */
    public static void onPageLoad() throws Exception {
        Settings_States.device = 1;
        DataModel.onPageLoad();
    }

}
