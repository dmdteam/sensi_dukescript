package com.sensicardiac.Record;


import static com.sensicardiac.Record.Login.Server_Login.Start_Login_Task;
import static com.sensicardiac.Record.Settings_States.*;
import static com.sensicardiac.Record.Stethoscope_Commands.Get_Paired_Scope;
import static com.sensicardiac.Record.Stethoscope_Commands.Scope_connect;
import static com.sensicardiac.Record.Stethoscope_Commands.pair_stethoscope;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.Property;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



@Model(className = "Data", targetId="", 
        properties = { @Property(name = "BTList",type = BTdev.class, array = true),
            @Property(name = "stet1",type = int.class),
            @Property(name = "selected", type = String.class, array = true),
            @Property(name = "stet_connected_label", type = String.class),
            @Property(name = "data_write_sec", type = String.class),
            @Property(name = "graph_data_x", type = String.class),
            @Property(name = "graph_data_y", type = String.class),
            @Property(name = "data_get", type = boolean.class),
            @Property(name = "progress_updates", type = int.class),
            @Property(name = "bt_icon", type = String.class),
            @Property(name = "play_back_method", type = String.class),
            //UserInfo for Display
            @Property(name = "ui_User_name", type = String.class),
            @Property(name = "ui_User_Sub", type = String.class),
            @Property(name = "ui_User_Acc", type = String.class),
            @Property(name = "ui_User_Proc", type = String.class),
            @Property(name = "ui_User_Expire", type = String.class),
})
final class DataModel {
    private static Data ui;
    public static Stethoscope_Commands stet = new Stethoscope_Commands();

    
    
    /**
     * Called when the page is ready.
     */
    @Model(className = "BTdev",properties = {
        @Property(name = "serial",type = String.class)})
    public static class BTModel{}
    
    @Function static void Stet_name_val(){
       
      List<String> selected1 = ui.getSelected(); 
      System.out.println("Slelcted: "+selected1);
      if (ui.getSelected().isEmpty())
      {
          selected1.add(0,ui.getBTList().get(0).getSerial());}
        
      String ConnectedScope = selected1.get(0);
      connecting(ConnectedScope);
      
      if (Sender_Stethoscope.isConnected() == true){
      
      if (stet_connect_state == true){
          ui.setStet_connected_label("Disconnect");
           ui.setData_get(false);
           ui.setBt_icon("images/ic_bt_con.png");
      } else { 
          ui.setStet_connected_label("Connect");
          ui.setData_get(true);
           ui.setBt_icon("images/ic_bt_dis.png");
      }}else { 
          ui.setStet_connected_label("Connect");
          ui.setData_get(true);
           ui.setBt_icon("images/ic_bt_dis.png");
      }
    }  
     
    @Function static void Record_Function(){
      recording(); 
      
    }  
    
    @Function static void Play_Function(){
           playing();
           
    }  
       
    @Function static void Stet_pair(){
       if (device == 1){
           ui.getBTList().clear();
           pair_stethoscope();
       }
       else if (device == 2)
       {
       ui.getBTList().clear();
      
                
       
       
               }
          Get_Paired_Scope();
       
         itr = pairedStethoscopes.iterator();
         while(itr.hasNext()) 
        {      
            Object element = itr.next();
            String test = element.toString();  
            ui.getBTList().add(new BTdev(test));
       
        }
      
       }
    @Function static void User_Info(){
    ui.setUi_User_name(ui_User_Name);
    
    }
    @Function static void Login_Function(){
    Start_Login_Task();
    
    TimerTask timeruiupde = new TimerTask() {
        @Override
        public void run() {
            ui.setUi_User_name(ui_User_Name);
    ui.setUi_User_Acc(ui_User_acc);
    ui.setUi_User_Expire(ui_User_expire);
        }
    };
    Timer timer = new Timer();
    timer.schedule(timeruiupde,0,500);
    }
    static void onPageLoad() throws Exception {
        ui = new Data(); 
       
        
        Populate_combobox();
        //Todo Move to Text Polulate
        ui.setUi_User_name(ui_User_Name);
        ui.applyBindings();
    }
    
    private static void Populate_combobox(){
       ui.setBt_icon("images/ic_bt_dis.png");
       ui.setStet_connected_label("Connect");
        Get_Paired_Scope();
        itr = pairedStethoscopes.iterator();
         while(itr.hasNext()) 
        {      
            Object element = itr.next();
            String test = element.toString();  
            ui.getBTList().add(new BTdev(test));
         
        }}
    
    private static void connecting(String ConnectedScope){
        
      Scope_connect(ConnectedScope);
    }
    
    private static void recording(){
        stet.Record_Timer();
    }
    
    private static void playing() {
        stet.Play_Timer();
        //stet.Playback_speaker();
    }
    
   

   
}
