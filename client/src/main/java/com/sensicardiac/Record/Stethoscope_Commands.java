/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sensicardiac.Record;


import android.media.MediaPlayer;
import com.mmm.healthcare.scope.ConfigurationFactory;
import static com.mmm.healthcare.scope.ConfigurationFactory.getBluetoothManager;
import static com.mmm.healthcare.scope.ConfigurationFactory.setLicenseFile;
import com.mmm.healthcare.scope.Errors;
import com.mmm.healthcare.scope.IBluetoothManager;
import com.mmm.healthcare.scope.IStethoscopeListener;
import com.mmm.healthcare.scope.Stethoscope;
import static com.sensicardiac.Record.Settings_States.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.nio.charset.StandardCharsets;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sensicardiac.Record.WaveHeader;
import java.nio.file.Path;
import java.util.Vector;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.omg.CORBA.Environment;



/**
 *
 * @author Edward
 */
public class Stethoscope_Commands {
    private static Timer stethoscopeUpdateTimer;
    private Thread recordTimer;
    private Thread playTimer;
    
    public static void Get_Paired_Scope(){
       License_IS = new ByteArrayInputStream(license_string.getBytes(StandardCharsets.UTF_8));
       conf = new ConfigurationFactory();
       setLicenseFile(License_IS);
       manager = getBluetoothManager();
       pairedStethoscopes = manager.getPairedDevices();
    }
    
    public static void Scope_connect(String ConnectedScope){
        if (stet_connect_state == false )
        {
        int x = 0;
        itr = pairedStethoscopes.iterator();
        while(itr.hasNext()){
            Object element = itr.next();
            String stethoscope_element = element.toString();
            if (stethoscope_element == null ? ConnectedScope == null : stethoscope_element.equals(ConnectedScope))
             {
                    Sender_Stethoscope = pairedStethoscopes.get(x);
                    try 
                    {
                        Sender_Stethoscope.connect();
                        
                        Stethoscope_Listener = addSendsenderListenererStethoscopeListener(Sender_Stethoscope);
                        createStethoscopeUpdateTimer();
                    }
                    catch (IOException ex)
                    {
                        // Logger.getLogger(Stet_Func.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                x++;   
            
        }
        stet_connect_state = true;
        }
         else 
        {
            Sender_Stethoscope.disconnect();
            
            Stethoscope_Listener = null;
           
            stet_connect_state = false;
        //    stethoscopeUpdateTimer.cancel();
        }
        if (Sender_Stethoscope.isConnected() == true){
                        is_Connected = true;}
        else
        if (Sender_Stethoscope.isConnected() == false){
                        is_Connected = false;}
    }
    
    private static IStethoscopeListener addSendsenderListenererStethoscopeListener(Stethoscope sender) {
       IStethoscopeListener listener = new IStethoscopeListener() {

			@Override
			public void mButtonDown(boolean isLongButtonClick) {

			}

			@Override
			public void mButtonUp() {

			}

			@Override
			public void plusButtonDown(boolean isLongButtonClick) {

			}

			@Override
			public void plusButtonUp() {
				
			}

			@Override
			public void minusButtonDown(boolean isLongButtonClick) {

			}

			@Override
			public void minusButtonUp() {

			}

			@Override
			public void filterButtonDown(boolean isLongButtonClick) {

			}

			@Override
			public void filterButtonUp() {

			}

			@Override
			public void onAndOffButtonDown(boolean isLongButtonClick) {

			}

			@Override
			public void onAndOffButtonUp() {

			}

			@Override
			public void lowBatteryLevel() {

			}

			@Override
			public void disconnected() {
                            
                            //TODO: Fix
				//onSenderDisconnectClick();
			}

			@Override
			public void error(Errors error, String message) {

			}

			@Override
			public void endOfOutputStream() {

			}

			@Override
			public void endOfInputStream() {

			}

			@Override
			public void outOfRange(boolean isOutOfRange) {

			}

			@Override
			public void underrunOrOverrunError(boolean isUnderrun) {
                             
			}
                        
                       
			
		};
		sender.addStethoscopeListener(listener);
		return listener;
	} 
    
    private static void createStethoscopeUpdateTimer() {
        TimerTask timerTask = new TimerTask() {

			@Override
			public void run() {
				onUpdateStethoscope();
			}

            private void onUpdateStethoscope() {               
              // bytes_getBytesReadPerSecond = String.valueOf(Sender_Stethoscope.getBytesReadPerSecond());
            }
		};

		stethoscopeUpdateTimer = new Timer();
		stethoscopeUpdateTimer.scheduleAtFixedRate(timerTask, 0, 10);
    }
    
    
    
    public void Record_Timer(){
        recordTimer = new Thread(){
        public void run(){
        record();
        }
        };
        recordTimer.start();
       // recordTimer.stop();
    }
     private int packtorec = 620;
      private void record() {
       
       if (Sender_Stethoscope.isConnected() == true){
           
           int numberOfAudioPacketsToReceive =  packtorec;
       //    senderStethoscope.getAudioOutputStream();
           Sender_Stethoscope.startAudioInput();
           isRecording=true;
           stethoscopeAudioData = new byte [128 * numberOfAudioPacketsToReceive];
           byte [] packet = new byte[128];
           offSet = 0;
         
           int receivedPackets = 0;
         //  recording_start_bytes =  senderStethoscope.getTotalBytesRead();
           while (receivedPackets < numberOfAudioPacketsToReceive)
           {
               int read;
               try 
               {
                   read = Sender_Stethoscope.getAudioInputStream().read(packet, 0, packet.length);
                   if (read > 0)
                   {
                       for (int k = 0; k < packet.length; k++) 
                       {
                            // Add the received packet to the audio data.
                           stethoscopeAudioData[k + offSet] = packet[k];  
                       }
                       receivedPackets++;
                       offSet += packet.length;}    
               }
               catch (Exception ex) 
               {
                
               }
           }
           Sender_Stethoscope.stopAudioInput();
           isRecording = false;
           File_Writer();
       }
    }

    
    
    public void Play_Timer(){
        playTimer = new Thread(){
        public void run(){
        play();}
        };
        playTimer.start();
    }
    
    private byte[] playpacket = new byte[128 *  packtorec];
private void play(){
     Play_Spk.Playback_spk();
//    if (device == 1){
//    Play_Spk.Playback_spk();}
//    else if (device == 2){
//     Play_And_Spk();
//        }
        if (Sender_Stethoscope.isConnected() == true){
           playpacket = stethoscopeAudioData;
           Sender_Stethoscope.startAudioOutput();
          // playback_start_bytes = senderStethoscope.getTotalBytesWritten();
          //  play_stop = true;
           int read = 0;
            try {
              //  while (read < playpacket.length/128){
               Sender_Stethoscope.getAudioOutputStream().write(playpacket, 0, playpacket.length);
         //  read++;
       //         }
                sleep(10000);
        } catch (IOException | InterruptedException ex) {
            
            }
            Sender_Stethoscope.stopAudioOutput();
            
        }
    }
    public static void pair_stethoscope(){
        if(getBluetoothManager().hasBluetoothAntenna()) {
			// Progress bar to display while we wait for stethoscopes to be paired.
        
			// Pair stethoscopes on another thread so we don't lockup the UI thread.
			Thread pairThread = new Thread() {
				public void run() { 
                                   // dlg.setVisible(true);
                                    
					// Get the bluetooth manager used to pair stethoscopes.
					IBluetoothManager manager = getBluetoothManager();
					
					// Do the actual pairing of the stethoscopes.
					Vector<Stethoscope> stethoscopes = manager.pairDevices();
					
					// Get the full list of paired stethoscopes after pairing is complete.
					stethoscopes = manager.getPairedDevices();
					
					// Update the list of stethoscopes.
					Settings_States.pairedStethoscopes = null;
                                        pairedStethoscopes = stethoscopes;
					
					// close the progress bar modal dialog.
					
                                       // dlg.setVisible(false);
				}
			};
	
			// Start the pairing process.
			pairThread.start();
                        while (pairThread.isAlive() == true){
                       
                        }
		} else {
			
			
		}
	}
    
    static File dir;
    
    private static void File_Writer() {
        if (device == 1){
       dir = new File("./Sensi_TEST/");
        try{
       if (dir.exists()==false){
       dir.mkdirs();}}catch (Exception e){
        System.out.println(e);}
         wavfile = new File(dir,"wav.wav");
       if (wavfile.exists()) {
            wavfile.delete();
            try {
                wavfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                 System.out.println(e);
            }
        } else {
            try {
                wavfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                 System.out.println(e);
            }
        }
        }
        else if (device == 2){
             File path = new File("/sdcard/Sensi_Test/");
             wavfile = new File(path,"wav.wav");
             if (wavfile.exists()) {
            wavfile.delete();
            try {
                wavfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                 System.out.println(e);
            }
        } else {
            try {
                path.mkdirs();
                wavfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                 System.out.println(e);
            }
        }
        }
       
      
       FileOutputStream audout;
        try {
            audout = new FileOutputStream(wavfile, true);
             WaveHeader hdr = new WaveHeader(WaveHeader.FORMAT_PCM, (short) 1, 4000, (short) 16, stethoscopeAudioData.length);
              hdr.write(audout);
              audout.write(stethoscopeAudioData);
               audout.close();
               System.out.println("Done Record");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Stethoscope_Commands.class.getName()).log(Level.SEVERE, null, ex);
            
        } catch (IOException ex) {
            Logger.getLogger(Stethoscope_Commands.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
//    private static void Play_And_Spk(){
//    
//         MediaPlayer mp = new MediaPlayer();
//        mp.setVolume(1.0f, 1.0f);
//
//        //throws IO exception
//        try {
//            
//            
//            mp.setDataSource(wavfile.toString());
//            mp.prepare();
//            mp.setLooping(false);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        mp.start();
//
//
//        //----------
//
//        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//                //button_voice_record.setVisibility(View.VISIBLE);
//                mp.stop();
//                mp.reset();
//                mp.release();
//               // Playing = false;
//               // button_voice_play.setText("Play");
//
//            }
//        });
//   }
    
}
//InputStream b_in = new ByteArrayInputStream(resultArray);

        
     
       