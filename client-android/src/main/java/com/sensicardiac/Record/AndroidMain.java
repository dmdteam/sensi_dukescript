package com.sensicardiac.Record;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass.Device;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import static android.content.ContentValues.TAG;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
//public class AndroidMain {
//    private AndroidMain() {
//    }
public class AndroidMain extends Activity {
    public AndroidMain() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    
       try {
           Intent android_bt_intent = new Intent(getApplicationContext(), Class.forName("com.dukescript.presenters.Android"));
            
           startActivity(android_bt_intent);
          
        } catch (ClassNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
        finish();
    }
    
    
    public static void main(String... args) throws Exception {
        Settings_States.device = 2;
       // bt();
        DataModel.onPageLoad();
    }
    
    
}
